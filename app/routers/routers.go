package routers

import (
	_ "assignment/docs"
	"log"

	"assignment/app/handler"

	"github.com/gin-gonic/gin"
	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func InitRouter() {
	OrderHandlers := handler.NewOrderHandler()
	r := gin.Default()

	r.POST("/Order", OrderHandlers.CreateOrder)
	r.PUT("/Order", OrderHandlers.CreateOrder)
	r.GET("/Order", OrderHandlers.GetOrderList)
	r.GET("/Order/:order_id", OrderHandlers.GetOrderDetail)
	r.DELETE("/Order/:order_id", OrderHandlers.DeleteOrder)
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))

	log.Println("=========== Server has been started ===========")
	r.Run(":8000")
}
