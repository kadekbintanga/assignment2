package models

import (
	"time"

	"gorm.io/gorm"
)



type Order struct {
	gorm.Model
	ID           uint      `json:"order_id" gorm:"primary_key"`
	CustomerName string    `json:"customer_name"`
	OrderedAt    time.Time `json:"ordered_at" gorm:"autoCreateTime"`
	Items        []Item    `gorm:"foreignKey:OrderID"`
}

type Item struct {
	gorm.Model
	ID          *uint  `json:"item_id" gorm:"primary_key"`
	ItemCode    string `json:"item_code"`
	Description string `json:"description"`
	Quantity    uint   `json:"quantity"`
	OrderID     uint   `json:"order_id"`
	Order       Order  `gorm:"foreignKey:OrderID"`
}
