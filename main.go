package main

import (
	"assignment/app/routers"
	"assignment/config"

	"gorm.io/gorm"
)

var (
	db *gorm.DB = config.ConnectDB()
)

func main() {
	config.MigrateDatabase(db)
	defer config.DisconnectDB(db)
	routers.InitRouter()
}
